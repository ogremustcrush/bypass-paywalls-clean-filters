// ==UserScript==
// @name            Bypass Paywalls Clean - nl/be
// @version         2.7.0.2
// @downloadURL     https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.nl.user.js
// @updateURL       https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.nl.user.js
// @match           *://*.nl/*
// @match           *://*.be/*
// ==/UserScript==

(function() {
  'use strict';

if (matchDomain('telegraaf.nl')) {
  waitDOMElement('div.MeteringNotification__backdrop', 'DIV', removeDOMElement, true);
}

window.setTimeout(function () {

var nl_mediahuis_region_domains = ['gooieneemlander.nl', 'haarlemsdagblad.nl', 'ijmuidercourant.nl', 'leidschdagblad.nl', 'noordhollandsdagblad.nl'];
var nl_pg_domains = ['demorgen.be', 'humo.be', 'parool.nl', 'trouw.nl', 'volkskrant.nl'];
var domain;

if (matchDomain('ftm.nl')) {
  let banner_pp = document.querySelector('div.banner-pp');
  removeDOMElement(banner_pp);
}

else if (matchDomain('groene.nl')) {
  setCookie('rlist', '', '', '/', 0);
}

else if (matchDomain(['gva.be', 'hbvl.be', 'nieuwsblad.be'])) {
  let paywall = document.querySelector('div[data-cj-root="subscription-wall"]');
  if (paywall) {
    removeDOMElement(paywall);
    let main_content = document.querySelector('div[data-mht-block="article-detail__article-main"]');
    let json_script = main_content.querySelector('script');
    let json_str = json_script.text.substring(json_script.textContent.indexOf('{'));
    try {
      let json = JSON.parse(json_str);
      if (json) {
        let json_text = Object.values(json)[0]['data']['article']['body'];
        let parser = new DOMParser();
        let div_content = main_content.querySelector('div');
        let par_elem, par_key, par_li, par_html, par_link;
        let head = document.querySelector('head');
        let streamone = false;
        let flourish = false;
        for (let par of json_text) {
          for (let key in par) {
            par_elem = document.createElement('p');
            par_key = par[key];
            if (['p', 'subhead'].includes(key)) {
              if (par_key.includes('<')) {
                par_html = parser.parseFromString('<p>' + par_key + '</p>', 'text/html');
                par_elem = par_html.querySelector('p');
              } else
                par_elem.innerText = par_key;
              if (key === 'subhead')
                par_elem.setAttribute('style', 'font-weight: bold;');
            } else if (key === 'image') {
              par_elem = document.createElement('img');
              par_elem.src = par_key.url;
            } else if (key === 'bullet_list') {
              par_elem = document.createElement('ul');
              for (let bullet of par_key) {
                par_html = parser.parseFromString('<li>' + bullet + '</li>', 'text/html');
                par_li = par_html.querySelector('li');
                let bullet_link = par_li.querySelector('a');
                if (bullet_link && bullet_link.href && !bullet_link.innerText)
                  bullet_link.innerText = bullet_link.href;
                par_elem.appendChild(par_li);
              }
            } else if (key === 'related') {
              par_elem = document.createElement('p');
              if (par_key.article && par_key.article.title && par_key.article.webcmsRelativeUrl) {
                par_link = document.createElement('a');
                if (par_key.article.label)
                  par_link.innerText = par_key.article.label;
                par_link.innerText += par_key.article.title;
                par_link.href = par_key.article.webcmsRelativeUrl;
                par_elem.appendChild(par_link);
              }
            } else if (key === 'iframe_sized') {
              par_elem = document.createElement('iframe');
              par_elem.src = par_key.url;
              if (par_key.height && par_key.width) {
                par_elem.setAttribute('height', par_key.height);
                par_elem.setAttribute('width', par_key.width);
              }
            } else if (key === 'streamone') {
              if (!streamone) {
                let streamone_script = document.createElement('script');
                streamone_script.setAttribute('src', 'https://shared.mediahuis.be/videoplayers/mediahuis/video-theoplayer.js?v=20220525T184101');
                streamone_script.setAttribute('defer', true);
                streamone_script.setAttribute('crossorigin', 'anonymous');
                if (head)
                  head.appendChild(streamone_script);
                streamone = true;
              }
              let par_key_id = par_key.id;
              par_html = parser.parseFromString('<div id="json_id"><div><div><div><div data-testid="embed-video"><div><div id="video-player-' + par_key_id + '" style="width:100%;" data-video-embed-id="' + par_key_id + '" data-video-target-id="video-player-' + par_key_id + '" data-video-brand="gva" class="js-theoplayer-placeholder"></div></div></div></div></div>', 'text/html');
              par_elem = par_html.querySelector('div');
            } else if (key === 'legacy-ml') {
              par_html = parser.parseFromString('<div>' + par_key + '</div>', 'text/html');
              par_elem = par_html.querySelector('div');
              if (!flourish && par_key.includes('flourish.studio')) {
                let flourish_script = document.createElement('script');
                flourish_script.setAttribute('src', 'https://public.flourish.studio/resources/embed.js');
                if (head)
                  head.appendChild(flourish_script);
                flourish = true;
              }
            } else {
              console.log(key);
              console.log(par_key);
              par_html = parser.parseFromString('<p>' + par_key + '</p>', 'text/html');
              par_elem = par_html.querySelector('p');
            }
            if (!['streamone', 'legacy-ml', 'iframe_sized'].includes(key))
              par_elem.setAttribute('style', 'font-size: 16px;');
            if (par_elem)
              div_content.appendChild(par_elem);
          }
        }
      }
    } catch (err) {
      console.warn('unable to parse text');
      console.warn(err);
    }
  }
  window.setTimeout(function () {
    let overlay = document.querySelector('div.cj-root');
    removeDOMElement(overlay);
    let noscroll = document.querySelector('html.is-dialog-active');
    if (noscroll)
      noscroll.classList.remove('is-dialog-active');
  }, 500); // Delay (in milliseconds)
}

else if (matchDomain(['knack.be', 'levif.be'])) {
  let paywall = document.querySelector('#paywall-modal');
  if (paywall) {
    removeDOMElement(paywall);
    document.querySelector('html').setAttribute('style', 'overflow-y: visible !important');
  }
}

else if (matchDomain(nl_mediahuis_region_domains)) {
  window.setTimeout(function () {
    let close_button = document.querySelector('button[data-testid="button-close"]');
    if (close_button)
      close_button.click();
    let premium = document.querySelector('div.common-components-plus_pluslabel--container');
    if (premium) {
      let hidden_article = document.querySelector('div[data-auth-body="article"]');
      if (hidden_article)
        hidden_article.removeAttribute('style');
      let paywall = document.querySelector('div[data-auth-root="paywall"]');
      removeDOMElement(paywall);
      let auth_body = document.querySelector('div[data-auth-body="article"]');
      if (paywall && auth_body) {
        let auth_body_par_count = auth_body.querySelectorAll('p');
        if (auth_body_par_count.length < 2) {
          let json_script = document.querySelector('script[data-fragment-type="PacoArticleContent"]');
          let json_str = json_script.text.substring(json_script.textContent.indexOf('{'));
          try {
            let json = JSON.parse(json_str);
            let article = Object.values(json)[0]['data']['article']['body'];
            auth_body.innerHTML = '';
            let par_html, par_dom, par_elem, par_div, par_key;
            let parser = new DOMParser();
            for (let par of article) {
              for (let key in par) {
                par_dom = document.createElement('p');
                par_elem = '';
                par_key = par[key];
                if (key === 'subhead') {
                  par_html = parser.parseFromString('<div><strong>' + par_key + '</strong></div>', 'text/html');
                  par_elem = par_html.querySelector('div');
                } else if (key === 'twitter' || key === 'instagram') {
                  par_elem = document.createElement('a');
                  Object.assign(par_elem, {
                    href: par_key,
                    innerText: par_key.split('?')[0],
                    target: '_blank'
                  });
                } else if (key === 'youtube') {
                  par_elem = document.createElement('iframe');
                  Object.assign(par_elem, {
                    src: 'https://www.youtube.com/embed/' + par_key.id,
                    id: 'ytplayer',
                    type: 'text/html',
                    width: 640,
                    height: 360,
                    frameborder: 0
                  });
                } else if (key === 'streamone') {
                  par_elem = document.createElement('iframe');
                  Object.assign(par_elem, {
                    src: 'https://content.tmgvideo.nl/embed/item=' + par_key.id,
                    type: 'text/html',
                    width: 640,
                    height: 360,
                    frameborder: 0
                  });
                } else if (key === 'image') {
                  par_elem = document.createElement('div');
                  let par_img = document.createElement('img');
                  par_img.src = par_key.url;
                  par_elem.appendChild(par_img);
                  par_div = document.createElement('div');
                  par_div.innerText = par[key].caption ? par[key].caption : '';
                  par_div.innerText += par[key].credit ? '\n' + par[key].credit : '';
                  par_elem.appendChild(par_div);
                } else {
                  par_html = parser.parseFromString('<p style="font-size: 18px; line-height: 1.625;">' + par_key + '</div>', 'text/html');
                  par_elem = par_html.querySelector('p');
                }
                if (par_elem)
                  par_dom.appendChild(par_elem);
                auth_body.appendChild(par_dom);
              }
            }
          } catch (err) {
            console.warn('unable to parse text');
            console.warn(err);
          }
        }
      }
    }
  }, 500);
}

else if (matchDomain(nl_pg_domains)) {
  setCookie('TID_ID', '', '', '/', 0);
  let banners = document.querySelectorAll('div[data-temptation-position^="PAGE_"], div[class^="ad--"]');
  let paywall = document.querySelectorAll('[data-temptation-position^="ARTICLE_"]');
  removeDOMElement(...banners, ...paywall);
  window.setTimeout(function () {
    let elem_hidden = document.querySelectorAll('[class^="artstyle__"][style="display: none;"]');
    for (let elem of elem_hidden)
      elem.removeAttribute('style');
  }, 500);
}

else if (matchDomain('nrc.nl')) {
  setCookie('counter', '', '', '/', 0);
  let mijnnrc_overlay = document.querySelector('#mijnnrc__modal__overlay');
  let subscribe_bar = document.querySelector('.header__subscribe-bar');
  removeDOMElement(mijnnrc_overlay, subscribe_bar);
  let paywall = document.querySelector('.has-paywall');
  if (paywall)
    paywall.classList.remove('has-paywall');
  let paywall_overlay = document.querySelector('.has-paywall-overlay');
  if (paywall_overlay)
    paywall_overlay.classList.remove('has-paywall-overlay');
}

else if (matchDomain('telegraaf.nl')) {
  setCookie('page_count', '', '', '/', 0);
  if (window.location.href.startsWith('https://www.telegraaf.nl/error?ref=/')) {
    window.setTimeout(function () {
      window.location.href = window.location.href.split('&')[0].replace('error?ref=/', '');
    }, 500);
  }
  let refresh = document.querySelector('div[id="content"] > meta[http-equiv="refresh"]');
  if (refresh) {
    window.setTimeout(function () {
      window.location.reload(true);
    }, 500);
  }
  let article_wrapper = document.querySelector('.ArticlePageWrapper__uid');
  let spotx_banner = document.querySelector('.ArticleBodyBlocks__inlineArticleSpotXBanner');
  removeDOMElement(spotx_banner);
  let premium = document.querySelector('.PremiumLabelWithLine');
  let article_id = article_wrapper ? article_wrapper.innerText : '123';
  let article_body_done = window.location.pathname.startsWith('/video/') || document.querySelector('#articleBody' + article_id);
  if (premium && !article_body_done) {
    let article_body_old = document.querySelector('[id^="articleBody"]');
    removeDOMElement(article_body_old);
    let html = document.documentElement.outerHTML;
    let json = html.includes('window.__APOLLO_STATE__=') ? html.split('window.__APOLLO_STATE__=')[1].split('};')[0] + '}' : '';
    if (json) {
      let json_article_id = json.split('uid\":')[1].split(/\D/)[0];
      if (json_article_id && json_article_id !== article_id) {
        window.setTimeout(function () {
          window.location.reload(true);
        }, 500);
      }
      let json_text = json.includes('"body":"') ? json.split('"body":"')[1].split('","__typename":')[0] : '';
      if (json_text) {
        let intro = document.querySelector('span[id^="articleIntro"]');
        if (intro)
          json_text = json_text.replace(intro.innerText + '\n\n', '');
        let article_body = document.querySelector('section[data-element="articleBody"]');
        if (article_body) {
          let div_main = document.createElement('div');
          div_main.setAttribute('id', 'articleBody' + article_id);
          let div_elem = document.createElement('div');
          div_elem.setAttribute('data-element', 'articleBodyBlocks');
          let text_array = json_text.split('\\n');
          text_array.forEach(p_text => {
            let p_div = document.createElement('p');
            p_div.setAttribute('class', 'ArticleBodyBlocks__paragraph');
            p_div.innerText = p_text;
            div_elem.appendChild(p_div);
          });
          div_main.appendChild(div_elem);
          article_body.insertBefore(div_main, article_body.firstChild);
        }
      }
    }
  }
}

else if (matchDomain('vn.nl')) {
  let paywall = document.querySelector('div.content__message-no-access-container');
  if (paywall) {
    let content_restriction = document.querySelector('div.content__restriction');
    removeDOMElement(paywall, content_restriction);
    let body = document.querySelector('body');
    if (body)
      body.style = 'height:auto !important;';
    let article_content = document.querySelector('section[data-article-content-element]');
    if (article_content)
      article_content.style = 'max-height:none !important;';
    let json_url_dom = document.querySelector('link[rel="alternate"][type="application/json"]');
    if (json_url_dom) {
      let json_url = json_url_dom.href;
      fetch(json_url)
      .then(response => {
        if (response.ok) {
          response.json().then(json => {
            let json_text = json.content.rendered;
            let content = document.querySelector('div[data-article-content-target]');
            if (json_text && content) {
              let parser = new DOMParser();
              let doc = parser.parseFromString('<div data-article-content-target>' + json_text + '</div>', 'text/html');
              let content_new = doc.querySelector('div');
              content.parentNode.replaceChild(content_new, content);
            }
          });
        }
      });
    }
  }
}

}, 1000);

// General Functions

function matchDomain(domains, hostname) {
  var matched_domain = false;
  if (!hostname)
    hostname = window.location.hostname;
  if (typeof domains === 'string')
    domains = [domains];
  domains.some(domain => (hostname === domain || hostname.endsWith('.' + domain)) && (matched_domain = domain));
  return matched_domain;
}

function setCookie(name, value, domain, path, days) {
  window.localStorage.clear();
  var max_age = days * 24 * 60 * 60;
  document.cookie = name + "=" + (value || "") + "; domain=" + domain + "; path=" + path + "; max-age=" + max_age;
}

function removeDOMElement(...elements) {
  for (let element of elements) {
    if (element)
      element.remove();
  }
}

function waitDOMElement(selector, tagName = '', callback, multiple = false) {
  new window.MutationObserver(function (mutations) {
    for (let mutation of mutations) {
      for (let node of mutation.addedNodes) {
        if (!tagName || (node.tagName === tagName)) {
          if (node.matches(selector)) {
            callback(node);
            if (!multiple)
              this.disconnect();
          }
        }
      }
    }
  }).observe(document, {
    subtree: true,
    childList: true
  });
}

})();
